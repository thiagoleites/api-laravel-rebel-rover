<?php

namespace Database\Factories;

use App\Models\{Category, User};
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = fake()->sentence(10);

        return [
            'cat_id'    => Category::all()->random(),
            'user_id'   => User::all()->random(),
            'title'     => $title,
            'subtitle'  => fake()->sentence(5),
            'slug'      => Str::slug($title),
            'image'     => fake()->imageUrl(),
            'content'   => fake()->text(180),
            'is_active' => fake()->boolean(),
        ];
    }
}
