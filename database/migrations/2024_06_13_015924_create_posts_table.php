<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cat_id')->references('id')->on('categories');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('title', 200);
            $table->string('subtitle')->nullable();
            $table->string('slug');
            $table->string('image')->nullable();
            $table->longText('content');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
};
