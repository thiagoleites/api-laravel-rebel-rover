default: env prepare up key-generate populate
.PHONY: env
env:
	@echo "--> Copying .env.example to .env file"
	@cp .env.example .env

.PHONY: prepare
prepare:
	@echo "--> Installing composer dependencies..."
	@sh ./bin/prepare.sh

.PHONY: up
up:
	@echo "--> Starting all docker containers..."
	@./vendor/bin/sail up --force-recreate -d
	@./vendor/bin/sail art storage:link

.PHONY: down
down:
	@echo "--> Stopping all docker containers..."
	@./vendor/bin/sail down

.PHONY: key-generate
key-generate:
	@echo "--> Generating new laravel key..."
	@./vendor/bin/sail artisan key:generate

.PHONY: populate
populate:
	@echo "--> Populating all table and data of project..."
	@./vendor/bin/sail artisan migrate:fresh --seed

.PHONY: restart
restart: down up
