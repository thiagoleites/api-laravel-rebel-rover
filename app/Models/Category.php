<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Collection, Model};

class Category extends Model
{
    use HasFactory;

    protected $table = "categories";

    protected $fillable = ['name', 'description'];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * Return all categories
     */

    public static function listCategories(): Collection
    {
        return self::query()->where('is_active', 1)->get();
    }
}
