<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory;

    protected $table = "posts";

    protected $guarded = [];

    // public static function slug(): Attribute
    // {
    //     return Attribute::make(
    //         set: fn (string $value) => $this->normalizeSlug($value),
    //     );
    // }

    // private function normalizeSlug(string $value): string
    // {
    //     $slug = Str::slug($value);

    //     $count = Post::query()
    //         ->where('slug', $slug)
    //         ->when($this->id, function ($query) {
    //             $query->where('id', '!=', $this->id);
    //         })
    //         ->count();

    //     if($count > 0) {
    //         return $slug . "-" . $count++;
    //     }

    //     return $slug;
    // }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug']  = Str::slug($value);
    }

    public static function listPosts($limit)
    {
        return self::query()->where('is_active', 1)->get();
    }
}
