<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {
        $arrPost = Post::listPosts(10);
        return response()->json($arrPost);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'cat_id' => 'required',
            'user_id' => 'required',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'slug' => 'required|string',
            'image' => 'nullable|string',
            'content' => 'required'
        ]);

        $posts = Post::create($data);

        return response()->json($posts, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        return response()->json($post);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        $validate = $request->validate([
            'cat_id' => 'required',
            'user_id' => 'required',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'slug' => 'required|string',
            'image' => 'nullable|string',
            'content' => 'required'
        ]);

        $post->update($validate);

        return response()->json($post, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete($post);

        return response()->json(null, 204);
    }
}
