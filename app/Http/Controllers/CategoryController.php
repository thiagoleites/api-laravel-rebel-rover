<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\{JsonResponse, Request};

class CategoryController extends Controller
{
    public function index(): JsonResponse
    {
        $arrCategory = Category::listCategories();
        return response()->json($arrCategory);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name'        => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);

        $category = Category::create($data);

        return response()->json($category, 201);
    }

    public function show(Category $category)
    {
        return response()->json($category);
    }

    public function edit(Category $category)
    {
    }

    public function update(Request $request, Category $category)
    {
        $validate = $request->validate([
            'name'        => 'required|string',
            'description' => 'nullable|string',
        ]);

        $category->update($validate);

        return response()->json($category, 200);
    }

    public function destroy(Category $category)
    {
        $category->delete($category);

        return response()->json(null, 204);
    }
}
