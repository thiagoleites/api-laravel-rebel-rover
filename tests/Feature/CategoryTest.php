<?php

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class); //import for testing with memory database - do migrate for test

it('can list categories', function () {
    Category::factory()->count(10)->create(['is_active' => 1]);
    $response = $this->getJson("/api/categories");
    $response->assertStatus(200)
        ->assertJsonCount(10);
});

it('should create a new category', function () {
    $data = [
        'name'        => 'Nova Categoria',
        'description' => 'Nova Categoria a ser criada',
    ];
    $response = $this->postJson("/api/categories", $data);

    $response->assertStatus(201)
        ->assertJsonFragment($data);

    $this->assertDatabaseHas('categories', $data);
});

it('can show a category', function () {
    $category = Category::factory()->create();
    $response = $this->getJson("/api/categories/{$category->id}");
    $response->assertStatus(200)
        ->assertJsonFragment(['name' => $category->name]);
});

it('should be able to edit an category', function () {

    $category = Category::factory()->create();

    $data = ['name' => 'Categoria Atualizada'];

    $response = $this->putJson("/api/categories/{$category->id}", $data);

    $response->assertStatus(200)
        ->assertJsonFragment($data);

    $this->assertDatabaseHas('categories', $data);
});

it('can delete a category', function () {
    $category = Category::factory()->create();
    $response = $this->deleteJson("/api/categories/{$category->id}");
    //    dd($response);
    $response->assertStatus(204);
    $this->assertDatabaseMissing('categories', ['id' => $category->id]);
});
