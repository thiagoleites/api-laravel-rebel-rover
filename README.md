<h1 align="center">Projeto: Landing Page com Dashboard</h1>

<p align="center">
    <img src="https://img.shields.io/badge/Php-8.3.3-informational" alt="stack php">
    <img src="https://img.shields.io/badge/Laravel-11-informational&color=brightgreen" alt="stack laravel">
</p>

## Descrição
Este projeto consiste em uma landing page desenvolvida com React para o front-end e Laravel para o back-end. A aplicação inclui um dashboard para gerenciamento de posts, cateorias, usuários e comentários.

## Estrutura do Projeto
- Frontend: React
- Backend: Laravel
- Banco de dados: MySQL


## Funcionalidades

### Frontend
- Exibição de posts e categorias na landing page.
- Formulário de contato.
- Interface de login e registro de usuários.

### Backend
- Autenticação e autorização de usuários.
- CRUD (Create, Read, Update, Delete) de posts.
- CRUD de categorias.
- CRUD de usuários.
- CRUD de comentários.

### Pré-requisitos
- Docker

Configuração do Ambiente de Desenvolvimento
Backend (Laravel)

1 - Baixando o projeto
```bash
# Clone o projeto através da url abaixo:
git clone https://gitlab.com/thiagoleites/api-laravel-rebel-rover.git
```

2 - Instalar e configurar as dependências
```bash
# Executar o comando make / make prepare
make
```
